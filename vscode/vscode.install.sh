SNIPPETS=$(mac "$HOME/Library/Application Support/Code/User/snippets" "$HOME/.config/Code/User/snippets")

install() {
    mkdir -p "$(dirname "$SNIPPETS")"
    dootlink snippets "$SNIPPETS"
}

uninstall() {
    dootrestore "$SNIPPETS"
}
