# Martin's Bash functions
# =======================

# addpath adds a path to the current $PATH and exports it.
# If the path is invalid or already exists in the $PATH, it will be ignored.
addpath() {
	if [ -d "$1" ]; then
		if [[ ":$PATH:" != *":$1:"* ]]; then
			export PATH="$1:$PATH"
		fi
	fi
}

mkcd() {
    mkdir -p $1 && cd $1
}

sourceifexists() {
	if [ -e "$1" ]; then source "$1"; fi
}

# === Platform helpers ====

platform() {
	if [ $(uname) = "Darwin" ]; then
		echo "mac"
	else
		echo "linux"
	fi
}

linux() {
	if [ "$1" ]; then
		if [ $(platform) = "linux" ]; then echo "$1"; else echo "$2"; fi
	else
		[ $(platform) = "linux" ]
		return $?
	fi
}

mac() {
	if [ "$1" ]; then
		if [ $(platform) = "mac" ]; then echo "$1"; else echo "$2"; fi
	else
		[ $(platform) = "mac" ]
		return $?
	fi
}
