# Martin's Bash
# =============
export DOOTROOT=`cat ~/.dootfiles`

source $DOOTROOT/shell/bash_functions.sh
addpath ~/Applications
addpath ~/bin
addpath $DOOTROOT/bin

source $DOOTROOT/shell/sdks.sh # SDK paths and bootstraps for RVM etc. are defined here
source $DOOTROOT/shell/prompt.sh # Git-enabled shell prompt

export CODE="$HOME/Code/"
alias cdcode="cd $CODE"
alias ..="cd .."
alias ...="cd ../.."
alias ....="cd ../../.."
alias ~="cd ~"

alias d=docker
alias g=git
alias bashrc="$EDITOR ~/.bashrc && source ~/.bashrc"
alias zshrc="$EDITOR ~/.zshrc && source ~/.zshrc"

export EDITOR="vim -f"

mac && alias ls="ls -G" # Enable colours
alias la="ls -a"
alias ll="ls -l"
alias lla="ls -la"
