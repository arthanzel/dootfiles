arthanzel does dootfiles
========================
Hello! These are my dootfiles. There are many others like them, but these ones are mine.

Usage
-----

    ./install.sh (install|uninstall)

If there are file conflicts, the installer will ask you whether you want to backup, overwrite, or skip the original file. When uninstalling, backups are automatically restored.

The installer will also create a `~/.dootfiles` file that contains the full path of the Dootfiles repository. The variable `$DOOTROOT`, which is defined in `shell/bashrc.sh`, will also reflect this location.

**For safety, always review the dootfiles for shifty code. Do not install the dootfiles if you doubt their safety or authenticity.**

Extending
---------
This repository is broken up into "modules", each living in its own subdirectory. Each module has an install script ending in `install.sh`. A module install script has two magic functions, `install` and `uninstall`, which are invoked in the working directory of the module.

Additionlly, the installer sources `dootlib.sh` and `shell/bash_functions.sh`, so any functions or variables defined there are available to module install scripts.

API
---
You can use these functions in module install scripts.

- `dootlink <source> <link>`: Installs a dootfile by creating a symbolic link like `<link> -> <source>`. `<source>` is automatically resolved to an absolute path. It can handle both file and directory links. If `<link>` already exists, `dootlink` prompts to backup, overwrite, or skip the file.
- `dootrestore <link>`: Restores a dootfile to its original state by unlinking the dootfile and restoring the backup, if it exists.
- `isdootlink <link>`: Returns 0 if `<link>` is a symbolic link pointing to somewhere within the Dootfiles repository. Otherwise, returns 1.
- `platform`: Outputs `mac` if on MacOS, `linux` otherwise.
