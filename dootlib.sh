# This file is meant to be sourced in a Bash script, NOT Zsh.

buildtime() {
    echo $(git log -1 --format=%cd --date=format:'%Y-%m-%dT%H:%M:%S')
}

isdootlink() {
    [[ -L "$1" && $(readlink "$1") == "$DOOTROOT"* ]]
    return $?
}

dootlink() {
    local SOURCE="$(realpath "$1")"
    local LINK="$2"

    if [ ! -e "$LINK" ]; then
        # Target file does not exist
        ln -s "$SOURCE" "$LINK"
    elif isdootlink "$LINK"; then
        # Target file already links LINK a dootfile
        :
    else
        # Target file should be overwritten
        PROMPT="[b]ackup, [o]verwrite, [s]kip?"

        echo "File already exists: $LINK"
        read -n 1 -p "$PROMPT" REPLY; echo
        while true; do
            case $REPLY in
                b) mv "$LINK" "${LINK}.old";;
                o) break;;
                s) return;;
            esac
        done

        rm -r "$LINK"
        ln -s "$SOURCE" "$LINK"
    fi
}

dootrestore() {
    FILE="$1"
    BACKUPFILE="${FILE}.old"

    if isdootlink "$FILE"; then
        rm "$FILE"
    fi

    if [ -e "$BACKUPFILE" ]; then
        mv "$BACKUPFILE" "$FILE"
    fi
}
