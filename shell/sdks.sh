# Android
if mac; then addpath ~/Applications/android/platform-tools; fi

# Java and friends
# export SDKMAN_DIR="/Users/martin/.sdkman"
# [[ -s "/Users/martin/.sdkman/bin/sdkman-init.sh" ]] && source "/Users/martin/.sdkman/bin/sdkman-init.sh"

# Go
# export GOROOT=/opt/go/1.8
# addpath $GOROOT/bin
# export GOPATH="$HOME/Code/go"
# addpath $GOPATH/bin

# Node
# Prefer n to nvm since n is lighter and doesn't slow down startup
export N_PREFIX=$HOME/.n
addpath $N_PREFIX/bin

# Ruby
# Prefer chruby and ruby-install, since they're lightweight
