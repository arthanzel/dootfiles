install() {
    dootlink bashrc.sh ~/.bashrc
    dootlink bash_profile.sh ~/.bash_profile
}

uninstall() {
    dootrestore ~/.bashrc
    dootrestore ~/.bash_profile
}
