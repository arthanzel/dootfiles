#!/usr/bin/env bash

# Make sure we are in the dootfiles directory so the build doesn't do screwey things.
# Do this by testing the presence of a random string guaranteed to be in this file.
if ! grep -q "IyEvYmluL2Jhc2gKCiMgTWF" install.sh; then
	echo "Must be in the dootfiles directory to run this script."
	exit 1
fi

MODE="$1"
DOOTROOT=$(realpath .)
INSTALLFILES=$(find . -mindepth 2 -maxdepth 2 -name "*install.sh")

LIBFILES=(shell/bash_functions.sh dootlib.sh)
for LIBFILE in "${LIBFILES[@]}"; do
    source "$LIBFILE"
done

echo "Dootfiles revision $(buildtime)"

runscript() {
    (
        ARG="$1"
        DIR=$(dirname "$ARG")
        FILE=$(basename "$ARG")

        echo "Running install script for $DIR"

        # Shim install methods
        shopt -s expand_aliases
        install()   { echo "Error: install() function missing in $ARG"; }
        uninstall() { echo "Error: uninstall() function missing in $ARG"; }

        cd "$DIR"
        source "$FILE"
        $MODE
    )
}

if [ "$MODE" = "install" ]; then
    echo "$DOOTROOT" > $HOME/.dootfiles
    for FILE in $INSTALLFILES; do
        runscript "$FILE"
    done
elif [ "$MODE" = "uninstall" ]; then
    for FILE in $INSTALLFILES; do
        runscript "$FILE"
    done
    rm $HOME/.dootfiles
else
    echo "Usage: ./install.sh (install|uninstall)"
fi
