install() {
    dootlink gitconfig ~/.gitconfig
    dootlink gitignore_global ~/.gitignore_global
}

uninstall() {
    dootrestore ~/.gitconfig
    dootrestore ~/.gitignore_global
}
