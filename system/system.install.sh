install() {
    dootlink tmux.conf ~/.tmux.conf
}

uninstall() {
    dootrestore ~/.tmux.conf
}
